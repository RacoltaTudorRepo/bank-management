package test;
import model.*;
import control.*;
import static org.junit.Assert.*;

import java.util.ArrayList;

public class Test {
	Bank banca=new Bank();
	Person p=new Person("Gigi","Str.Mamaia",25);
	SpendingAccount cont=new SpendingAccount(3,5000);
	ArrayList<Account> conturi=null;
	double suma=cont.getSuma();
	
	@org.junit.Test
	public void test() {
		banca.addPerson(p);
		banca.addAccount(cont, p);
		conturi=banca.getDate().get(p);
		cont=(SpendingAccount) conturi.get(0);
		cont.add(500);
		assertTrue((suma+500)==cont.getSuma());
	}

}
