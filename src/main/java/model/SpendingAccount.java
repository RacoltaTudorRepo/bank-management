package model;

public class SpendingAccount extends Account{
	public SpendingAccount(int id, int suma){
		super(id,suma);
		type="SpendingAccount";
	}
	public void withdraw(int suma) {
		if(suma>this.suma) System.out.println("Nu sunt destui bani");
		else this.suma-=suma;
	}
	public String toString() {
		return "Contul de cumparaturi nr "+ id+ " contine "+ suma+" lei";
	}
	public void add(int suma) {
		this.suma+=suma;
	}
}
