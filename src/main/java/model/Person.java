package model;

public class Person implements java.io.Serializable{
	private String nume;
	private String adresa;
	private int varsta;
	public Person(String nume,String adresa, int varsta) {
		this.adresa=adresa;
		this.nume=nume;
		this.varsta=varsta;
	}
	public String getNume() {
		return nume;
	}
	public String getAdresa() {
		return adresa;	
	}
	public int getV() {
		return varsta;
	}
	public boolean equals(Object o) { //OVERRIDE EQUALS PENTRU STERGERE
		if (o==this) return true;
		if(!(o instanceof Person)) 
			return false;
		Person per=(Person)o;
		return nume.equals(per.getNume());
	}
	public void setNume(String nume) {
		this.nume=nume;
	}
	public void setAdresa(String adresa) {
		this.adresa=adresa;
	}
	public void setV(int varsta) {
		this.varsta=varsta;
	}
}
