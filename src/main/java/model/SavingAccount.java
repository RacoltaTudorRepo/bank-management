package model;

public class SavingAccount extends Account{
	public SavingAccount(int id, double suma){
		super(id,suma);
		type="SavingAccount";
	}
	public void withdraw(int suma) {
		if(suma>this.suma) System.out.println("Nu sunt destui bani");
		if(suma<1000) System.out.println("Suma prea mica");
		else this.suma-=suma;
	}
	public void interest() {//3% dobanda pe conturi
		this.suma=this.suma+(double)this.suma*(3.0/100);
	}
	public String toString() {
		return "Contul de econimii nr "+ id+ " contine "+ suma+" lei";
	}
	public void add(int suma) {
		this.suma+=suma;
	}
}
