package model;

public class Account implements java.io.Serializable{
	protected int id;
	protected double suma;
	protected String type;
	public Account(int id, double suma) {
		this.id=id;
		this.suma=suma;
	}
	public int getId() {
		return id;
	}
	public boolean equals(Account account) {
		if(this.id==account.id) return true;
		else return false;
	}
	public double getSuma() {
		return suma;
	}
	public String getType() {
		return type;
	}
}
