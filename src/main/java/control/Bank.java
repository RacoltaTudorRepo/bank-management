package control;
import java.util.*;
import model.*;
public class Bank implements BankProc,java.io.Serializable{
	private Hashtable<Person,ArrayList<Account>> date;
	public Bank() {
		date=new Hashtable<Person,ArrayList<Account>>();
	}
	public void addPerson(Person person) {
		assert(person!=null);
		ArrayList<Account> conturi =new ArrayList<Account>();
		date.put(person, conturi);		
	}
	public void removePerson(Person p) {
		assert(p!=null);
		date.remove(p);

	}
	public void addAccount(Account account, Person person) {
		assert(account!=null);
		assert(person!=null);
		date.get(person).add(account);
	}
	public void removeAccount(int id,Person person) {
		assert(id>0);
		assert(person!=null);
		ArrayList<Account> conturi=date.get(person);
		for(Account a:conturi)
			if(a.getId()==id)
				{date.get(person).remove(a);
				return;
				}
	}
	public Account getAccount(int id, Person person) {
		assert(id>0);
		assert(person!=null);
		ArrayList<Account> conturi=date.get(person);
		Account cont=null;
		for(Account a:conturi)
			if(a.getId()==id) cont=a;
		return cont;
	}
	public Hashtable<Person,ArrayList<Account>> getDate(){
		assert(date!=null);
		return date;
	}
}
