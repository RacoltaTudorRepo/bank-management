package control;
import model.*;
public interface BankProc {
	public void addPerson(Person person);
	public void removePerson(Person person);
	public void addAccount(Account account,Person person);
	public void removeAccount(int id,Person person);
	public Account getAccount(int id, Person person);

}
