package control;

public class Observer {
	public static void notify(int id, String nume) {
		System.out.println("S-au efectuat modificari asupra contului cu id-ul : "+ id+ " al carui titular este : "+nume);
	}
}
