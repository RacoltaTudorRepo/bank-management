package view;
import model.*;
import control.*;
import control.Observer;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.*;
import java.awt.event.ActionListener;
import java.io.*;
import java.awt.event.ActionEvent;

public class GUIProiect {
	private Bank banca;
	private Object[][] data;
	private JFrame frame;
	private JTable table;
	private DefaultTableModel model;
	private String[] columns= {"Nume","Adresa","Varsta"};
	private JTable table1;
	private DefaultTableModel model1;
	private String[] columns1= {"ID","Tip","Suma","Nume proprietar"};
	private JTextField introducere;
	private JTextField stergereP;
	private JTextField actualizareP;
	private JTextField identifP;


	private JTextField introducereC;
	private JTextField nUpdateC;
	private JTextField iUpdateC;
	private JTextField sumaU;
	private JTextField nStergereU;
	private JTextField iStergereU;;

	public GUIProiect() {
		banca=new Bank();
		frame = new JFrame();
		frame.setTitle("Bank Management");
		frame.setBounds(100, 100, 1184, 827);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(78, 356, 300, 300);
		frame.getContentPane().add(scrollPane);
		table = new JTable();
		model = new DefaultTableModel(data,columns);
		table.setModel(model);
		table.setAutoCreateRowSorter(true);//click listener
		scrollPane.setViewportView(table);
		
		table.addMouseListener(new java.awt.event.MouseAdapter()//click listener
		{
			public void mouseClicked(java.awt.event.MouseEvent e)
			{
				int row=table.rowAtPoint(e.getPoint());
				int col= table.columnAtPoint(e.getPoint());
				String date="";
				for(col=0;col<3;col++)
					date+=table.getValueAt(row, col).toString()+" ";
				JOptionPane.showMessageDialog(null,"Continutul dorit: " +date);
			};
		});


		JScrollPane scrollPane1 = new JScrollPane();
		scrollPane1.setBounds(603, 356, 392, 300);
		frame.getContentPane().add(scrollPane1);
		table1 = new JTable();
		model1 = new DefaultTableModel(data,columns1);
		table1.setModel(model1);
		table1.setAutoCreateRowSorter(true);//click listener
		scrollPane1.setViewportView(table1);

		introducere = new JTextField();
		introducere.setBounds(78, 55, 190, 20);
		frame.getContentPane().add(introducere);
		introducere.setColumns(10);

		JLabel lblIntroducerePersoana = new JLabel("Introducere persoana");
		lblIntroducerePersoana.setBounds(117, 30, 166, 14);
		frame.getContentPane().add(lblIntroducerePersoana);

		//ADD PERSON
		JButton btnConfirm = new JButton("Confirm");
		btnConfirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				model.setRowCount(0);
				String aux=introducere.getText();
				String[] splitat=aux.split(",");
				Person client= new Person(splitat[0],splitat[1],Integer.parseInt(splitat[2]));
				banca.addPerson(client);//Adaugare in banca
				refreshPerson();
			}
		});

		btnConfirm.setBounds(299, 54, 89, 23);
		frame.getContentPane().add(btnConfirm);

		stergereP = new JTextField();
		stergereP.setBounds(78, 144, 190, 20);
		frame.getContentPane().add(stergereP);
		stergereP.setColumns(10);


		//DELETE PERSON
		JLabel lblStergerePersoana = new JLabel("Stergere persoana");
		lblStergerePersoana.setBounds(117, 118, 207, 14);
		frame.getContentPane().add(lblStergerePersoana);

		JButton btnConfirm_1 = new JButton("Confirm");
		btnConfirm_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				model.setRowCount(0);
				String aux=stergereP.getText();
				String[] splitat=aux.split(",");
				Person client= new Person(splitat[0],splitat[1],Integer.parseInt(splitat[2]));
				Enumeration<Person> pers=banca.getDate().keys();
				while(pers.hasMoreElements()==true) {
					Person p=pers.nextElement();
					if(p.getNume().equals(client.getNume()))
						banca.removePerson(p);//Stergere din banca
				}
				refreshPerson();
				refreshAccount();
			}
		});
		btnConfirm_1.setBounds(299, 143, 89, 23);
		frame.getContentPane().add(btnConfirm_1);

		actualizareP = new JTextField();
		actualizareP.setBounds(117, 219, 151, 20);
		frame.getContentPane().add(actualizareP);
		actualizareP.setColumns(10);

		JLabel lblActualizareDate = new JLabel("Actualizare date");
		lblActualizareDate.setBounds(143, 194, 125, 14);
		frame.getContentPane().add(lblActualizareDate);

		//UPDATE PERSON
		JButton btnConfirm_2 = new JButton("Confirm");
		btnConfirm_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				model.setRowCount(0);
				String aux=identifP.getText();
				String aux1=actualizareP.getText();
				String[] splitat=aux1.split(",");
				Person client= new Person(splitat[0],splitat[1],Integer.parseInt(splitat[2]));
				Enumeration<Person> pers=banca.getDate().keys();
				while(pers.hasMoreElements()==true) {
					Person p=pers.nextElement();
					if(p.getNume().equals(aux))
					{p.setNume(client.getNume());
					p.setAdresa(client.getAdresa());
					p.setV(client.getV());
					}
				}
				refreshPerson();
			}
		});
		btnConfirm_2.setBounds(299, 218, 89, 23);
		frame.getContentPane().add(btnConfirm_2);

		identifP = new JTextField();
		identifP.setBounds(21, 219, 86, 20);
		frame.getContentPane().add(identifP);
		identifP.setColumns(10);

		JLabel lblNewLabel = new JLabel("Identificare");
		lblNewLabel.setBounds(32, 194, 89, 14);
		frame.getContentPane().add(lblNewLabel);

		introducereC = new JTextField();
		introducereC.setBounds(603, 70, 252, 20);
		frame.getContentPane().add(introducereC);
		introducereC.setColumns(10);

		JLabel lblAdaugareCont = new JLabel("Adaugare cont");
		lblAdaugareCont.setBounds(704, 45, 151, 14);
		frame.getContentPane().add(lblAdaugareCont);

		JLabel addMoney = new JLabel("");
		addMoney.setBounds(586, 222, 46, 14);
		frame.getContentPane().add(addMoney);

		JRadioButton radioSaving = new JRadioButton("Saving Account");
		radioSaving.setBounds(603, 97, 151, 23);
		frame.getContentPane().add(radioSaving);

		JRadioButton radioSpending = new JRadioButton("Spending Account");
		radioSpending.setBounds(603, 118, 190, 23);
		frame.getContentPane().add(radioSpending);
		ButtonGroup group = new ButtonGroup();
		group.add(radioSaving);
		group.add(radioSpending);

		//ADD ACCOUNT
		JButton btnConfirm_3 = new JButton("Confirm");
		btnConfirm_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				model1.setRowCount(0);
				String aux1=introducereC.getText();
				String[] splitat=aux1.split(",");
				String nume=splitat[0];
				Account cont=null;
				if(radioSaving.isSelected()==true)
					cont=new SavingAccount(Integer.parseInt(splitat[1]),Integer.parseInt(splitat[2]));
				else if(radioSpending.isSelected()==true)
					cont=new SpendingAccount(Integer.parseInt(splitat[1]),Integer.parseInt(splitat[2]));	
				Enumeration<Person> pers=banca.getDate().keys();
				while(pers.hasMoreElements()==true) {
					Person p=pers.nextElement();
					if(p.getNume().equals(nume))
					{banca.addAccount(cont, p);
					}
				}
				refreshAccount();
			}
		});
		btnConfirm_3.setBounds(906, 69, 89, 23);
		frame.getContentPane().add(btnConfirm_3);

		nUpdateC = new JTextField();
		nUpdateC.setBounds(603, 191, 86, 20);
		frame.getContentPane().add(nUpdateC);
		nUpdateC.setColumns(10);

		iUpdateC = new JTextField();
		iUpdateC.setBounds(727, 191, 86, 20);
		frame.getContentPane().add(iUpdateC);
		iUpdateC.setColumns(10);

		sumaU = new JTextField();
		sumaU.setBounds(849, 191, 86, 20);
		frame.getContentPane().add(sumaU);
		sumaU.setColumns(10);

		JLabel lblNumeTitular = new JLabel("Nume titular");
		lblNumeTitular.setBounds(603, 166, 86, 14);
		frame.getContentPane().add(lblNumeTitular);

		JLabel lblIdCont = new JLabel("Id cont");
		lblIdCont.setBounds(747, 166, 46, 14);
		frame.getContentPane().add(lblIdCont);

		JLabel lblSuma = new JLabel("Suma");
		lblSuma.setBounds(874, 166, 46, 14);
		frame.getContentPane().add(lblSuma);

		JRadioButton rdbtnAdd = new JRadioButton("Add");
		rdbtnAdd.setBounds(603, 218, 109, 23);
		frame.getContentPane().add(rdbtnAdd);

		JRadioButton withdrawMoney = new JRadioButton("Withdraw");
		withdrawMoney.setBounds(603, 243, 109, 23);
		frame.getContentPane().add(withdrawMoney);
		ButtonGroup group1 = new ButtonGroup();
		group1.add(withdrawMoney);
		group1.add(rdbtnAdd);
		
		
		//UPDATE ACCOUNT
		JButton updateButton = new JButton("Confirm");
		updateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nume=nUpdateC.getText();
				String idCont=iUpdateC.getText();
				int suma=Integer.parseInt(sumaU.getText());
				ArrayList<Account> conturi=null;
				Account a=null;
				SavingAccount b=null;
				SpendingAccount c=null;
				Enumeration<Person> pers=banca.getDate().keys();
				while(pers.hasMoreElements()==true) {
					Person p=pers.nextElement();
					if(p.getNume().equals(nume)) {
						conturi=banca.getDate().get(p);
						int i;
						for(i=0;i<conturi.size();i++)
						{a=conturi.get(i);
						if(a.getId()==Integer.parseInt(idCont))
							if(a.getType().equals("SavingAccount"))
							{
								b=(SavingAccount)a;
								if(withdrawMoney.isSelected())
									b.withdraw(suma);
								else b.add(suma);
								if(suma>=1000) Observer.notify(b.getId(),p.getNume());
							}
							else {
								c=(SpendingAccount)a;
								if(withdrawMoney.isSelected())
									c.withdraw(suma);
								else c.add(suma);
								Observer.notify(c.getId(),p.getNume());
							}
						}
					}
				}
				refreshAccount();
			}
		});
		updateButton.setBounds(973, 190, 89, 23);
		frame.getContentPane().add(updateButton);
		
		//APPLY INTEREST
		JButton interestRate = new JButton("Apply interest");
		interestRate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Enumeration<Person> pers=banca.getDate().keys();
				ArrayList<Account> conturi=null;
				String nume=nUpdateC.getText();
				String idCont=iUpdateC.getText();
				Account a=null;
				SavingAccount b=null;
				while(pers.hasMoreElements()==true) {
					Person p=pers.nextElement();
					if(p.getNume().equals(nume)) {
						conturi=banca.getDate().get(p);
						int i;
						for(i=0;i<conturi.size();i++)
						{a=conturi.get(i);
						if(a.getId()==Integer.parseInt(idCont)) {
							if(a.getType().equals("SavingAccount")) {
								b=(SavingAccount)a;
								b.interest();
								Observer.notify(b.getId(),p.getNume());
							}
						}
						}
					}
				}
				refreshAccount();
			}
		});
		interestRate.setBounds(727, 229, 128, 23);
		frame.getContentPane().add(interestRate);

		JLabel lblnumeidsuma = new JLabel("(Nume,Id,Suma)");
		lblnumeidsuma.setBounds(760, 101, 95, 14);
		frame.getContentPane().add(lblnumeidsuma);

		nStergereU = new JTextField();
		nStergereU.setBounds(603, 310, 86, 20);
		frame.getContentPane().add(nStergereU);
		nStergereU.setColumns(10);

		iStergereU = new JTextField();
		iStergereU.setBounds(727, 310, 86, 20);
		frame.getContentPane().add(iStergereU);
		iStergereU.setColumns(10);
		
		//DELETE ACCOUNT
		JButton deleteAccount = new JButton("Confirm");
		deleteAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Enumeration<Person> pers=banca.getDate().keys();
				ArrayList<Account> conturi=null;
				String nume=nStergereU.getText();
				String idCont=iStergereU.getText();
				Account a=null;
				while(pers.hasMoreElements()==true) {
					Person p=pers.nextElement();
					if(p.getNume().equals(nume)) {
						conturi=banca.getDate().get(p);
						int i;
						for(i=0;i<conturi.size();i++)
						{a=conturi.get(i);
						if(a.getId()==Integer.parseInt(idCont)) {
							banca.removeAccount(Integer.parseInt(idCont), p);
							Observer.notify(Integer.parseInt(idCont),p.getNume());
						}
						}
					}
				}
				refreshAccount();
			}		
		});
		deleteAccount.setBounds(849, 309, 89, 23);
		frame.getContentPane().add(deleteAccount);

		JLabel label = new JLabel("Nume titular");
		label.setBounds(603, 288, 86, 14);
		frame.getContentPane().add(label);

		JLabel label_1 = new JLabel("Id cont");
		label_1.setBounds(747, 285, 46, 14);
		frame.getContentPane().add(label_1);
		
		
		//LOAD DATA
		JButton btnLoadData = new JButton("Load data");
		btnLoadData.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			      try {
			         FileInputStream fileIn = new FileInputStream("D:\\Proiecte Java\\pt2018_30226_racolta_tudor_assignment_4\\bank.ser");
			         ObjectInputStream in = new ObjectInputStream(fileIn);
			         banca = (Bank) in.readObject();
			         in.close();
			         fileIn.close();
			      } catch (IOException i) {
			         i.printStackTrace();
			         return;
			      } catch (ClassNotFoundException c) {
			         System.out.println("Employee class not found");
			         c.printStackTrace();
			         return;
			      }
					
				refreshAccount();
				refreshPerson();
			}
		});
		btnLoadData.setBounds(1020, 350, 109, 23);
		frame.getContentPane().add(btnLoadData);
		
		
		//SAVE DATA
		JButton btnSaveData = new JButton("Save data");
		btnSaveData.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
			         FileOutputStream fileOut = new FileOutputStream("D:\\\\Proiecte Java\\\\pt2018_30226_racolta_tudor_assignment_4\\\\bank.ser");
			         ObjectOutputStream out = new ObjectOutputStream(fileOut);
			         out.writeObject(banca);
			         out.close();
			         fileOut.close();
			      } catch (IOException i) {
			         i.printStackTrace();
			      }
			}
		});
		btnSaveData.setBounds(1020, 300, 106, 23);
		frame.getContentPane().add(btnSaveData);
	}

	public void refreshAccount() {
		model1.setRowCount(0);
		Enumeration<Person> pers=banca.getDate().keys();
		Object rowdata[]=new Object[4];
		while(pers.hasMoreElements()==true) {
			Person p=pers.nextElement();
			ArrayList<Account> conturi=banca.getDate().get(p);
			for(Account c: conturi) {
				rowdata[0]=new String(Integer.toString(c.getId()));
				rowdata[1]=new String(Double.toString(c.getSuma()));
				rowdata[2]=new String(c.getType());
				rowdata[3]=new String(p.getNume());
				model1.addRow(rowdata);
			}
		}
	}
	public void refreshPerson() {
		model.setRowCount(0);
		Object rowdata[]=new Object[3];
		Enumeration<Person> pers=banca.getDate().keys();
		while(pers.hasMoreElements()==true) {
			Person p=pers.nextElement();
			rowdata[0]=new String(p.getNume());
			rowdata[1]=new String(p.getAdresa());
			rowdata[2]=new String(Integer.toString(p.getV()));
			model.addRow(rowdata);
		}
	}
	public JFrame getFrame() {
		return frame;
	}
}
